# 10/24 - 機率與Python

## 機率(Probability)

Error和機率有很重要的關係

normal distribution: 鐘形分布（常態分佈）

P(x)的兩種類型

- 離散(discrete)：用直方圖表示，所有機率總和會是1
- 連續(continuous)：全部的

$ \displaystyle P(d_1,d_2)= \displaystyle \int_{d_1}^{d_2}f(x)dx$

機率分布方程式(probability distribution function, pdf)

眾數（mode）：峰值

中位數（medium）：”typical value”，找面積是整體50%的地方

平均數（mean）：要注意平均數和pdf之間的關係

- 寬度方法

## Python

### 指定運算子

![Untitled](1024-probability/Untitled.png)

在numpy中給一個整數齊次的array

```python
x=np.arange(4)
print(x)

#結果:[0 1 2 3]

v=np.arange(3,11,1) #從3開始到10，間隔為1
print(v)
#結果:[3,  4,  5,  6,  7,  8,  9, 10]

#numpy.linspace(start, stop, num=50, endpoint=True, retstep=False, dtype=None, axis=0)

xx=np.linspace(-1,1,20)
print(xx)
#結果會跑出-1到1之中等距的20個數字
```

### 統計圖繪製

使用matplotlib，可以使用LATEX文法

```python
import matplotlib.pyplot as plt
plt.plot(x,y)
```

即可繪製出xy的分布圖。

要畫多張圖的話，可以使用subplot指令

```python
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=(5,5))
#nrows:列長 ncols:欄寬 figsize:圖的大小

fig1, ax1 = plt.subplots(1,1,figsize=(6,6))
ax1.plot(x,y)
ax1.set_xlabel('x') #x軸的標籤
ax1.set_ylabel('y') #y軸的標籤
ax1.set_title(r'$ y=x^2 $' ) #用$號表示LATEX語法，r是用於轉譯

fig1.savefig("filename.png", dpi=300) #形成圖檔
```

課堂作業：畫出normal distribution

```python
import matplotlib.pyplot as plt
import numpy as np
x=np.linspace(10,36,100)
mu=23
sigma=4
p=(1/sigma*np.sqrt(2*np.pi))*np.exp((-1/2)*((x-mu)/sigma)**2)
fig, ax1 = plt.subplots(1,1,figsize=(8,5))
plt.plot(x,p)
ax1.set_xlabel('x')
ax1.set_ylabel('p(x)')
ax1.set_title('normal PDF , '+r'$ p(x)=\frac{1}{\sigma \sqrt{2 \pi}}e^{-\frac{1}{2}(\frac{x-\mu}{\sigma})^2} $')
fig.savefig("20221024_Quiz_Terry.pdf", dpi=300)
```

![Untitled](1024-probability/Untitled1.png)

想一次畫三條？可以！

```python
ax1.plot(x,y,x,y2,x,y3) #一組一組來
```

設定顏色參數必須導入模組：

```python
import matplotlib as mpl
from cycler import cycler
mpl.rcParams['axes.prop_cycle'] = cycler(color='bgrcmyk')
```

計算最大值、最小值、標準差與平均

```python
randomArray1 = np.random.randint(402,5203,1000)
RA_1=randomArray1/10

Bmean=np.mean(RA_1) #平均值
print(Bmean)
Bstd=np.std(RA_1) #標準差
print(Bstd)
Bmax=np.max(RA_1) #最大值
print(Bmax)
Bmin=np.min(RA_1) #最小值
print(Bmin)
```

繪製直方圖(histogram)

```python
fig, ax = plt.subplots(1,1,figsize=(10,6))
ax.hist(RA_1,bins = 10,density=False,color='c')
#bins:要取多少間距
ax.set_title('Histogram')
```

![Untitled](1024-probability/Untitled2.png)

### 讀檔

```python
grade=np.loadtxt('./score.d', unpack=True)
```

unpack:要不要輸入成一個ID array

開子圖

nrows和ncols在控制子圖數

```python
fig, ax = plt.subplots(nrows=3, ncols=2, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None)
```

用array的概念把圖畫在第3行第2列

```python
ax[2,1].hist(grade,10,color='r')
#記得pythonh從0開始算！
```

畫圖

```python
df.plot(x='ObsTime',y='Temperature',ax=ax1)
```

![Untitled](1024-probability/Untitled3.png)

畫散落圖

```python
df.plot.scatter(x='T Max',y='Temperature')
```

![Untitled](1024-probability/Untitled4.png)
